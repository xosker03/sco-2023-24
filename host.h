#ifndef HOST_H
#define HOST_H

#include <stdio.h>
#include <stdint.h>
#include "zaklad.h"

struct key {
    uint32_t key[8];
};

//First word == 0; first bit == 0
struct blockdata {
    uint32_t block[4]; //MSW to LSW
};
struct bytedata {
    uint8_t byte[16];
};

extern struct key key;
extern struct blockdata subkeys[33];
extern struct blockdata base_iv;


void host_init_key(struct key * k);
void host_init_iv(struct blockdata * oiv);


#endif
