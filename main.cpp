#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <omp.h>

#include "Serpent.h"
#include "GPUSerpent.h"
#include "Memmapper.h"


using namespace std;


uint8_t getRandom(){
    FILE *f = fopen("/dev/random", "r");
    if(f == NULL){
        throw 0;
    }
    int dat = fgetc(f);
    if(dat == EOF){
        throw 1;
    }
    fclose(f);
    return (uint8_t) dat;
}


int main (int argc, char** argv){
    std::string inputFile;
    std::string keyFile;
    std::string ivFile;
    std::string outputFile;

    bool encrypt_mode = true;
    bool generate_key = false;

    int option;
    while ((option = getopt(argc, argv, "i:k:v:o:dgh")) != -1) {
        switch (option) {
            case 'i':
                if(optarg)
                    inputFile = optarg;
                break;
            case 'k':
                if(optarg)
                    keyFile = optarg;
                break;
            case 'v':
                if(optarg)
                    ivFile = optarg;
                break;
            case 'o':
                if(optarg)
                    outputFile = optarg;
                break;
            case 'd':
                encrypt_mode = false;
                break;
            case 'g':
                generate_key = true;
                break;
            case 'h':
                printf("Nápověda\n");
                printf("\t-h --- nápověda\n");
                printf("\t-d --- dešifrovat\n");
                printf("\t-g --- vygenerovat klíč\n");
                printf("\t-i file --- vstupní soubor\n");
                printf("\t-o file --- výstupní soubor\n");
                printf("\t-k file --- soubor klíče\n");
                printf("\t-v file --- výstupní soubor inicializačního vektoru\n");
                exit(0);
                break;
            case '?':
                std::cerr << "Unknown option: -" << static_cast<char>(optopt) << std::endl;
                return 1;
            default:
                std::cerr << "Unexpected error while processing options." << std::endl;
                return 1;
        }
    }

    if(inputFile == ""){
        printf("Nezadal si vstupní soubor\n");
        exit(3);
    }
    if(keyFile == ""){
        printf("Nezadal si soubor klíče\n");
        exit(4);
    }
    if(ivFile == ""){
        printf("Nezadal si výstupní soubor inicializačního vektoru\n");
        exit(5);
    }
    if(outputFile == ""){
        printf("Nezadal si výstupní soubor\n");
        exit(6);
    }

    Memmapper keyMap(keyFile);
    if(generate_key && encrypt_mode){
        keyMap.create(32);
        char * kk = keyMap.data();
        for(int i = 0 ; i < 32; ++i){
            kk[i] = getRandom();
        }
    } else {
        keyMap.read();
    }
    if(keyMap.size() != 32){
        printf("Klíč má špatnou velikost %lu/%dB", keyMap.size(), 32);
        exit(1);
    }
    Memmapper ivMap(ivFile);
    if(encrypt_mode){
        ivMap.create(16);
        char * vv = ivMap.data();
        for(int i = 0 ; i < 16; ++i){
            vv[i] = getRandom();
        }
    } else {
        ivMap.read();
    }
    if(ivMap.size() != 16){
        printf("IV má špatnou velikost %lu/%dB", keyMap.size(), 32);
        exit(2);
    }

    host_init_key((struct key *)keyMap.data());
    host_init_iv((struct blockdata *)ivMap.data());
    init_gpu(0);


    char * mdat = NULL;
    char * mdatout = NULL;
    uint64_t msize = 0;

    Memmapper input(inputFile);
    Memmapper output(outputFile);

    input.read();
    output.create(input.size());

    mdat = input.data();
    msize = input.size();
    mdatout = output.data();

    gpu_crypt(msize, mdat, mdatout, 0);
    // Serpent cryptic(msize, mdat, mdatout, 0);
    // cryptic.encrypt();

    // init_gpu(1);
    // init_gpu(2);
    // init_gpu(3);
    // omp_set_num_threads(12);
    // uint64_t chunk = msize / 12;
    // chunk = chunk / 16 + (!!(chunk % 16));
    // chunk *= 16;
    //
    // #pragma omp parallel for
    // for(uint64_t i = 0; i < msize; i += chunk){
    //     char * dat = mdat;
    //     char * odat = mdatout;
    //
    //     uint64_t siz = chunk;
    //
    //     if(i + siz > msize){
    //         siz = msize - i;
    //     }
    //
    //     dat += i;
    //     odat += i;
    //
    //     printf("%lu -> %lu\n", i, i + siz);
    //     // gpu_crypt(siz, dat, odat, i / sizeof(struct blockdata), 0, i / chunk);
    //     Serpent cryptic(siz, dat, odat, i / sizeof(struct blockdata));
    //     cryptic.encrypt();
    // }


    output.mclose();
    input.mclose();
    keyMap.mclose();
    ivMap.mclose();

    return 0;
}





