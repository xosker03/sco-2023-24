#include "Serpent.h"

#include <vector>

#include "serpent-tables.h"
#include "zaklad.h"


inline void key_print(struct key * k);
inline void blockdata_print(struct blockdata * b, int idx);
inline void blockdata_inc(struct blockdata * b);
inline void blockdata_plus(struct blockdata * b, uint64_t x);

inline struct blockdata blockdata_xor(struct blockdata * l, struct blockdata * r);

inline void bitSwapCpy(uint32_t * d, uint32_t * s, unsigned di, unsigned si);
inline void blockBitSwapCpy(struct blockdata * dst, struct blockdata * src, unsigned dstIdx, unsigned srcIdx);
__always_inline uint32_t rotateLeft(uint32_t d, int i);
inline uint8_t blockBitGet(struct blockdata * src, unsigned srcIdx);
inline void blockBitSet(struct blockdata * dst, unsigned dstIdx, uint8_t bit);


inline void key_print(struct key * k){
    printf("%skey: %s0x", bc.BLUE, bc.VIOLET);
    for(int i = 0; i < 8; ++i)
        printf("%.8X", k->key[i]);
    printf("%s\n", bc.ENDC);
}

inline void blockdata_print(struct blockdata * b, int idx){
    printf("%sbl %d: %s0x", bc.CYAN, idx, bc.YELLOW);
    for(int i = 0; i < 4; ++i)
        printf("%.8X", b->block[i]);
    printf("%s\n", bc.ENDC);
}
inline void blockdata_inc(struct blockdata * b){
    uint64_t * d = (uint64_t *) b;
    if(d[0] + 1 == 0){
        ++d[1];
    }
    ++d[0];
}
inline void blockdata_plus(struct blockdata * b, uint64_t x){
    uint64_t * d = (uint64_t *) b;
    uint64_t tmp = d[0];
    d[0] += x;
    if(d[0] < tmp)
        ++d[1];
}
inline struct blockdata blockdata_xor(struct blockdata * l, struct blockdata * r){
    struct blockdata ret;
    ret.block[0] = l->block[0] ^ r->block[0];
    ret.block[1] = l->block[1] ^ r->block[1];
    ret.block[2] = l->block[2] ^ r->block[2];
    ret.block[3] = l->block[3] ^ r->block[3];
    return ret;
}




inline void bitSwapCpy(uint32_t * d, uint32_t * s, unsigned di, unsigned si) {
    uint32_t sval = !!(*s & (((uint32_t) 1) << si));
    *d &= ~(((uint32_t) 1) << di);
    *d |= sval << di;
}

inline void blockBitSwapCpy(struct blockdata * dst, struct blockdata * src, unsigned dstIdx, unsigned srcIdx){
    unsigned abdx = dstIdx >> 5; // ai/32
    unsigned bbdx = srcIdx >> 5; // bi/32

    unsigned aidx = dstIdx & 0b11111;
    unsigned bidx = srcIdx & 0b11111;

    bitSwapCpy(&dst->block[abdx], &src->block[bbdx], aidx, bidx);
}


__always_inline uint32_t rotateLeft(uint32_t d, int i) {
    u_int32_t left = d >> (32 - i);
    return (d << i) | left;
}

inline uint8_t blockBitGet(struct blockdata * src, unsigned srcIdx){
    uint8_t ret = 0;
    unsigned bldx = srcIdx >> 5;
    unsigned bitdx = srcIdx & 0b11111;
    ret = !!(src->block[bldx] & (((uint32_t) 1) << bitdx));
    return ret;
}
inline void blockBitSet(struct blockdata * dst, unsigned dstIdx, uint8_t bit){
    unsigned bldx = dstIdx >> 5;
    unsigned bitdx = dstIdx & 0b11111;
    dst->block[bldx] &= ~(((uint32_t) 1) << bitdx);
    dst->block[bldx] |= bit << bitdx;
}






Serpent::Serpent(uint64_t size, char * _data, char * _dataout, uint64_t ivOffset)
{
	set_data(size, _data);
    dataout = (struct blockdata *) _dataout;
    init(ivOffset);
}

Serpent::~Serpent()
{

}

void Serpent::set_data(uint64_t size, char* _data)
{
    data = (struct blockdata *) _data;
    data_block_count = (size * sizeof(char)) / sizeof(struct blockdata);
    end_bytes = (size * sizeof(char)) % sizeof(struct blockdata);

    // printf("%lu %lu %lu\n", size, sizeof(char), sizeof(struct blockdata));
    //
    // printviolet("blocks %lu\n", data_block_count);
    // printviolet("end bytes %lu\n", end_bytes);
}


inline struct blockdata Serpent::init_permut(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    struct blockdata ret;
    for(int i = 0; i < 128; ++i)
        blockBitSwapCpy(&ret, &block, i, IPTable[i]);
    return ret;
}

inline struct blockdata Serpent::subkey_apply(struct blockdata & inblock, struct blockdata & subkey)
{
    struct blockdata block = blockdata_xor(&inblock, &subkey);
    return block;
}

inline struct blockdata Serpent::sbox(struct blockdata & inblock, int idx)
{
    struct blockdata block;
    struct bytedata * in = (struct bytedata *) &inblock;
    struct bytedata * out = (struct bytedata *) &block;
    for(int i = 0; i < 16; ++i){
        uint8_t b = in->byte[i];
        uint8_t h = b >> 4;
        uint8_t l = b & 0b1111;
        h = SBox[idx][h];
        l = SBox[idx][l];
        out->byte[i] = (h << 4) | l;
    }
    return block;
}

inline struct blockdata Serpent::lin_trans(struct blockdata & inblock)
{
    struct blockdata ret;
    auto t = LTTable;

    int i, j;
    uint8_t b;
    for (i = 0; i < 128; i++) {
        b = 0;
        j = 0;
        while (t[i][j] != MARKER) {
            b ^= blockBitGet(&inblock, t[i][j]);
            j++;
        }
        blockBitSet(&ret, i, b);
    }
    return ret;
}

inline struct blockdata Serpent::round(struct blockdata & inblock, int idx)
{
    struct blockdata block = inblock;
    block = subkey_apply(block, subkeys[idx]);
    block = sbox(block, idx);
    block = lin_trans(block);
    return block;
}

inline struct blockdata Serpent::final_round(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    block = subkey_apply(block, subkeys[31]);
    block = sbox(block, 31);
    block = subkey_apply(block, subkeys[32]);
    return block;
}


inline struct blockdata Serpent::final_permut(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    struct blockdata ret;
    for(int i = 0; i < 128; ++i)
        blockBitSwapCpy(&ret, &block, i, FPTable[i]);
    return ret;
}



inline struct blockdata Serpent::crypt(struct blockdata & inblock, uint64_t ivIdx)
{
    struct blockdata block = inblock;
    blockdata_plus(&block, ivIdx);
    block = init_permut(block);
    for(int i = 0; i < 31; ++i){
        block = round(block, i);
    }
    block = final_round(block);
    block = final_permut(block);
    // blockdata_inc(&iv);
    return block;
}


void Serpent::init(uint64_t ivOffset)
{
    // printcyan("INIT\n");
    iv = base_iv;
    // key_print(&key);
    // blockdata_print(&iv, -1);
    blockdata_plus(&iv, ivOffset);
}


void Serpent::encrypt()
{
    // printblue("encrypt\n");
    for(uint64_t i = 0; i < data_block_count; ++i){
        struct blockdata cypher = crypt(iv, i);
        // blockdata_print(&cypher, i);
        if(i == data_block_count-1 && end_bytes){
            struct bytedata * bytecypher = (struct bytedata *) &cypher;
            struct bytedata * bytedata = (struct bytedata *) &data;
            struct bytedata * bytedataout = (struct bytedata *) &dataout;
            for(int j = 0; j < end_bytes; ++j)
                bytedataout[i].byte[j] = bytedata[i].byte[j] ^ bytecypher->byte[j];
        } else {
            for(int j = 0; j < 4; ++j)
                dataout[i].block[j] = data[i].block[j] ^ cypher.block[j];
        }
    }
}

void Serpent::decrypt()
{
    // printblue("decrypt\n");
    for(uint64_t i = 0; i < data_block_count; ++i){
        struct blockdata cypher = crypt(iv, i);
        if(i == data_block_count-1 && end_bytes){
            struct bytedata * bytecypher = (struct bytedata *) &cypher;
            struct bytedata * bytedata = (struct bytedata *) &data;
            struct bytedata * bytedataout = (struct bytedata *) &dataout;
            for(int j = 0; j < end_bytes; ++j)
                bytedataout[i].byte[j] = bytedata[i].byte[j] ^ bytecypher->byte[j];
        } else {
            for(int j = 0; j < 4; ++j)
                dataout[i].block[j] = data[i].block[j] ^ cypher.block[j];
        }
    }
}


