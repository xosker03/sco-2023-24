#include "host.h"
#include "serpent-tables.h"

#define GOLDEN_FRAC ((uint32_t) 0x9e3779b9)



struct key key;
struct blockdata subkeys[33];
struct blockdata base_iv;


void key_mixer();


static inline void bitSwapCpy(uint32_t * d, uint32_t * s, unsigned di, unsigned si) {
    uint32_t sval = !!(*s & (((uint32_t) 1) << si));
    *d &= ~(((uint32_t) 1) << di);
    *d |= sval << di;
}

static inline void blockBitSwapCpy(struct blockdata * dst, struct blockdata * src, unsigned dstIdx, unsigned srcIdx){
    unsigned abdx = dstIdx >> 5; // ai/32
    unsigned bbdx = srcIdx >> 5; // bi/32

    unsigned aidx = dstIdx & 0b11111;
    unsigned bidx = srcIdx & 0b11111;

    bitSwapCpy(&dst->block[abdx], &src->block[bbdx], aidx, bidx);
}
static __always_inline uint32_t rotateLeft(uint32_t d, int i) {
    u_int32_t left = d >> (32 - i);
    return (d << i) | left;
}
static struct blockdata init_permut(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    struct blockdata ret;
    for(int i = 0; i < 128; ++i)
        blockBitSwapCpy(&ret, &block, i, IPTable[i]);
    return ret;
}





void host_init_key(struct key * k){
    if(!k){
        key = (struct key) {
            0x88888888,
            0x77777777,
            0x66666666,
            0x55555555,
            0x44444444,
            0x33333333,
            0x22222222,
            0x11111111
        }; //FIXME
        throw 25;
    } else {
        key = *k;
    }

    key_mixer();
}
void host_init_iv(struct blockdata * oiv){
    if(oiv){
        base_iv = *oiv;
    } else {
        base_iv = (struct blockdata) {0x11111111,0x22222222,0x33333333,0x44444444};
        throw 25;
    }
}




typedef uint32_t keySchedule[33][WORDS_PER_BLOCK];
typedef uint32_t BLOCK[WORDS_PER_BLOCK];
typedef uint32_t KEY[WORDS_PER_KEY];
typedef uint32_t WORD;
__always_inline uint8_t getBitFromWord(uint32_t src, int bit){
    return !!(src & (((uint32_t) 1) << bit));
}
__always_inline uint8_t makeNibble(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3) {
    return (uint8_t) (b0 | (b1 << 1) | (b2 << 2) | (b3 << 3));
}
__always_inline uint8_t getBitFromNibble(NIBBLE x, int p) {
    return (uint8_t) ((x & (0x1 << p)) >> p);
}

static void makeSubkeys(KEY userKey, keySchedule K) {
    int r = 32;
    int i, j, l, whichS;
    NIBBLE input, output;
    WORD k[132], raw_w[140];
    WORD* w = &raw_w[8];

    for (i = -8; i < 0; i++) {
        w[i] = userKey[i+8];
    }

    for (i = 0; i < 132; i++) {
        w[i] = rotateLeft(w[i-8] ^ w[i-5] ^ w[i-3] ^ w[i-1] ^ GOLDEN_FRAC ^ i, 11);
    }

    for (i = 0; i < r+1; i++) {
        whichS = (r + 3 - i) % r;
        k[0+4*i] = k[1+4*i] = k[2+4*i] = k[3+4*i] = 0;
        for (j = 0; j < 32; j++) {
            input = makeNibble(getBitFromWord(w[0+4*i], j),
                               getBitFromWord(w[1+4*i], j),
                               getBitFromWord(w[2+4*i], j),
                               getBitFromWord(w[3+4*i], j));
            output = SBox[whichS][input];
            for (l = 0; l < 4; l++) {
                k[l+4*i] |= ((WORD) getBitFromNibble(output, l)) << j;
            }
        }
    }

    for (i = 0; i < 33; i++) {
        for (j = 0; j < 4; j++) {
            K[i][j] = k[4*i+j];
        }
    }
}


void key_mixer()
{
    makeSubkeys(*((KEY*) &key.key[0]), *((keySchedule*)&subkeys[0]));
    for (int i = 0; i < 33; i++) {
        subkeys[i] = init_permut(subkeys[i]);
    }
    return;
}
