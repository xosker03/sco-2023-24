/*
  $Id: serpent-tables.h,v 1.6 1998/06/10 13:51:47 fms Exp $

  # This file is part of the C reference implementation of Serpent.
  #
  # Written by Frank Stajano,
  # Olivetti Oracle Research Laboratory <http://www.orl.co.uk/~fms/> and
  # Cambridge University Computer Laboratory <http://www.cl.cam.ac.uk/~fms27/>.
  # 
  # (c) 1998 Olivetti Oracle Research Laboratory (ORL)
  #
  # Original (Python) Serpent reference development started on 1998 02 12.
  # C implementation development started on 1998 03 04.
  #
  # Serpent cipher invented by Ross Anderson, Eli Biham, Lars Knudsen.
  # Serpent is a candidate for the Advanced Encryption Standard.

  */

#ifndef _SERPENT_TABLES_
#define _SERPENT_TABLES_
 
// #include "serpent-api.h"
#define MAX_XOR_TAPS_PER_BIT 7 /* Maximum number of entries per row in the
                                Linear Transformation table, i.e. maximum
                                # of input bits to be xored together to
                                yield an output bit. */
#define BITS_PER_BYTE 8
#define BYTES_PER_WORD 4
#define WORDS_PER_BLOCK 4
#define WORDS_PER_KEY 8
#define BITS_PER_WORD (BITS_PER_BYTE*BYTES_PER_WORD)
#define BITS_PER_BLOCK (BITS_PER_WORD*WORDS_PER_BLOCK)

typedef unsigned char BYTE;
typedef unsigned char NIBBLE;
typedef int permutationTable[BITS_PER_BLOCK];
typedef BYTE xorTable[BITS_PER_BLOCK][MAX_XOR_TAPS_PER_BIT+1];

// EMBED_RCS(serpent_tables_h,
//           "$Id: serpent-tables.h,v 1.6 1998/06/10 13:51:47 fms Exp $")


/* Tables of constants */

/* Each row of this array corresponds to one S-box. Each S-box in turn is
   an array of 16 integers in the range 0..15, without repetitions. Having
   the value v (say, 14) in position p (say, 0) means that if the input to
   that S-box is the pattern p (0, or 0x0) then the output will be the
   pattern v (14, or 0xe). */
extern NIBBLE SBox[][16];
extern NIBBLE SBoxInverse[][16];

/* The Initial and Final permutations are each represented by an array
   containing the integers in 0..127 without repetitions.  Having value v
   (say, 32) at position p (say, 1) means that the output bit at position p
   (1) comes from the input bit at position v (32). Note that the two
   tables are the inverse of each other (IPTableInverse == FPTable).*/
extern permutationTable IPTable;
extern permutationTable FPTable;

/* The Linear Transformation is represented as an array of 128 rows, one
   for each output bit. Each one of the 128 rows, terminated by a MARKER
   which isn't part of the data, is composed of up to 7 integers in the
   range 0..127 specifying the positions of the input bits that must be
   XORed together (say, 72, 144 and 125) to yield the output bit
   corresponding to the position of that list (say, 1). */
#define MARKER 0xff
extern xorTable LTTable;
extern xorTable LTTableInverse;

#endif
