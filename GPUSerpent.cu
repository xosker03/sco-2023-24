#include "GPUSerpent.h"
#include "serpent-tables.h"
#include "zaklad.h"
#include "Serpent.h"

__constant__ NIBBLE GSBox[32][16];
__constant__ permutationTable GIPTable;
__constant__ permutationTable GFPTable;
__constant__ xorTable GLTTable;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__constant__ struct blockdata Gsubkeys[33];
__constant__ struct blockdata Gbase_iv;


class GPUSerpent{
public:
    __device__ GPUSerpent(uint64_t blcnt, struct blockdata * _data, uint64_t ivOffset);
    __device__ ~GPUSerpent();

    __device__ void init(uint64_t ivOffset);

    __device__ void encrypt();

private:
    __device__ inline struct blockdata crypt(struct blockdata & inblock, uint64_t ivIdx);
    __device__ inline struct blockdata round(struct blockdata & inblock, int idx = -1);
    __device__ inline struct blockdata subkey_apply(struct blockdata & inblock, struct blockdata & subkey);
    __device__ inline struct blockdata sbox(struct blockdata & inblock, int idx);
    __device__ inline struct blockdata lin_trans(struct blockdata & inblock);
    __device__ inline struct blockdata final_round(struct blockdata & inblock);
    __device__ inline struct blockdata init_permut(struct blockdata & inblock);
    __device__ inline struct blockdata final_permut(struct blockdata & inblock);


    struct blockdata iv;

    uint64_t data_block_count;
    struct blockdata * data;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// __device__ static inline void blockdata_inc(struct blockdata * b){
//     uint64_t * d = (uint64_t *) b;
//     if(d[0] + 1 == 0){
//         ++d[1];
//     }
//     ++d[0];
// }
__device__ static inline void blockdata_plus(struct blockdata * b, uint64_t x){
    uint64_t * d = (uint64_t *) b;
    uint64_t tmp = d[0];
    d[0] += x;
    if(d[0] < tmp)
        ++d[1];
}
__device__ static inline struct blockdata blockdata_xor(struct blockdata * l, struct blockdata * r){
    struct blockdata ret;
    ret.block[0] = l->block[0] ^ r->block[0];
    ret.block[1] = l->block[1] ^ r->block[1];
    ret.block[2] = l->block[2] ^ r->block[2];
    ret.block[3] = l->block[3] ^ r->block[3];
    return ret;
}




__device__ static inline void bitSwapCpy(uint32_t * d, uint32_t * s, unsigned di, unsigned si) {
    uint32_t sval = !!(*s & (((uint32_t) 1) << si));
    *d &= ~(((uint32_t) 1) << di);
    *d |= sval << di;
}

__device__ static inline void blockBitSwapCpy(struct blockdata * dst, struct blockdata * src, unsigned dstIdx, unsigned srcIdx){
    unsigned abdx = dstIdx >> 5; // ai/32
    unsigned bbdx = srcIdx >> 5; // bi/32

    unsigned aidx = dstIdx & 0b11111;
    unsigned bidx = srcIdx & 0b11111;

    bitSwapCpy(&dst->block[abdx], &src->block[bbdx], aidx, bidx);
}


// __device__ static __always_inline uint32_t rotateLeft(uint32_t d, int i) {
//     u_int32_t left = d >> (32 - i);
//     return (d << i) | left;
// }

__device__ static inline uint8_t blockBitGet(struct blockdata * src, unsigned srcIdx){
    uint8_t ret = 0;
    unsigned bldx = srcIdx >> 5;
    unsigned bitdx = srcIdx & 0b11111;
    ret = !!(src->block[bldx] & (((uint32_t) 1) << bitdx));
    return ret;
}
__device__ static inline void blockBitSet(struct blockdata * dst, unsigned dstIdx, uint8_t bit){
    unsigned bldx = dstIdx >> 5;
    unsigned bitdx = dstIdx & 0b11111;
    dst->block[bldx] &= ~(((uint32_t) 1) << bitdx);
    dst->block[bldx] |= bit << bitdx;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ GPUSerpent::GPUSerpent(uint64_t blcnt, struct blockdata * _data, uint64_t ivOffset)
{
    data = _data;
    data_block_count = blcnt;
    init(ivOffset);
}

__device__ GPUSerpent::~GPUSerpent()
{

}

__device__ inline struct blockdata GPUSerpent::init_permut(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    struct blockdata ret;
    for(int i = 0; i < 128; ++i)
        blockBitSwapCpy(&ret, &block, i, GIPTable[i]);
    return ret;
}

__device__ inline struct blockdata GPUSerpent::subkey_apply(struct blockdata & inblock, struct blockdata & subkey)
{
    struct blockdata block = blockdata_xor(&inblock, &subkey);
    return block;
}

__device__ inline struct blockdata GPUSerpent::sbox(struct blockdata & inblock, int idx)
{
    struct blockdata block;
    struct bytedata * in = (struct bytedata *) &inblock;
    struct bytedata * out = (struct bytedata *) &block;
    for(int i = 0; i < 16; ++i){
        uint8_t b = in->byte[i];
        uint8_t h = b >> 4;
        uint8_t l = b & 0b1111;
        h = GSBox[idx][h];
        l = GSBox[idx][l];
        out->byte[i] = (h << 4) | l;
    }
    return block;
}

__device__ inline struct blockdata GPUSerpent::lin_trans(struct blockdata & inblock)
{
    struct blockdata ret;
    auto t = GLTTable;

    int i, j;
    uint8_t b;
    for (i = 0; i < 128; i++) {
        b = 0;
        j = 0;
        while (t[i][j] != MARKER) {
            b ^= blockBitGet(&inblock, t[i][j]);
            j++;
        }
        blockBitSet(&ret, i, b);
    }
    return ret;
}

__device__ inline struct blockdata GPUSerpent::round(struct blockdata & inblock, int idx)
{
    struct blockdata block = inblock;
    block = subkey_apply(block, Gsubkeys[idx]);
    block = sbox(block, idx);
    block = lin_trans(block);
    return block;
}

__device__ inline struct blockdata GPUSerpent::final_round(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    block = subkey_apply(block, Gsubkeys[31]);
    block = sbox(block, 31);
    block = subkey_apply(block, Gsubkeys[32]);
    return block;
}


__device__ inline struct blockdata GPUSerpent::final_permut(struct blockdata & inblock)
{
    struct blockdata block = inblock;
    struct blockdata ret;
    for(int i = 0; i < 128; ++i)
        blockBitSwapCpy(&ret, &block, i, GFPTable[i]);
    return ret;
}



__device__ inline struct blockdata GPUSerpent::crypt(struct blockdata & inblock, uint64_t ivIdx)
{
    struct blockdata block = inblock;
    blockdata_plus(&block, ivIdx);
    block = init_permut(block);
    for(int i = 0; i < 31; ++i){
        block = round(block, i);
    }
    block = final_round(block);
    block = final_permut(block);
    // blockdata_inc(&iv);
    return block;
}


__device__ void GPUSerpent::init(uint64_t ivOffset)
{
    iv = Gbase_iv;
    // blockdata_print(&iv, -1);
    blockdata_plus(&iv, ivOffset);
}


__device__ void GPUSerpent::encrypt()
{
    for(uint64_t i = 0; i < data_block_count; ++i){
        struct blockdata cypher = crypt(iv, i);
        for(int j = 0; j < 4; ++j)
            data[i].block[j] ^= cypher.block[j];
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// __device__ static inline void blockdata_print(struct blockdata * b, int idx){
//     printf("bl %d: 0x", idx);
//     for(int i = 0; i < 4; ++i)
//         printf("%.8X", b->block[i]);
//     printf("\n");
// }

__global__ void cryptKernel(uint64_t block_count, struct blockdata * Gblock, uint64_t ivOffset) {
    uint64_t idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(idx < block_count){
        GPUSerpent cr(1, Gblock + idx, ivOffset + idx);
        cr.encrypt();
    }
}


void init_gpu(int gpu){
    cudaSetDevice(gpu);
    cudaMemcpyToSymbol(Gsubkeys, subkeys, 33 * sizeof(struct blockdata));
    cudaMemcpyToSymbol(Gbase_iv, &base_iv, sizeof(struct blockdata));

    cudaMemcpyToSymbol(GSBox, SBox, 32 * 16 * sizeof(NIBBLE));
    cudaMemcpyToSymbol(GIPTable, IPTable, sizeof(permutationTable));
    cudaMemcpyToSymbol(GFPTable, FPTable, sizeof(permutationTable));
    cudaMemcpyToSymbol(GLTTable, LTTable, sizeof(xorTable));
}


void gpu_crypt(uint64_t size, char * _data, char * dataout, uint64_t ivOffset, int priority, int gpu){
    cudaSetDevice(gpu);
    cudaStream_t stream;
    cudaStreamCreateWithPriority(&stream, cudaStreamDefault, priority);

    uint8_t * Gdata;
    cudaMalloc(&Gdata, size * sizeof(uint8_t));
    cudaMemcpyAsync(Gdata, _data, size * sizeof(uint8_t), cudaMemcpyHostToDevice, stream);

    uint64_t data_block_count = (size * sizeof(char)) / sizeof(struct blockdata);
    // uint64_t end_bytes = (size * sizeof(char)) % sizeof(struct blockdata);

    int numThreads = 1024;
    int blockSize = data_block_count / numThreads;
    blockSize = (blockSize == 0)?1:blockSize;
    printf("spuštím kernel pro %d bloků, celkem blockdata %lu, zbytek %lu\n", blockSize, data_block_count, size - data_block_count*sizeof(struct blockdata));
    cryptKernel<<<blockSize, numThreads, 0 ,stream>>>(data_block_count, (struct blockdata *) Gdata, ivOffset);
    cudaStreamSynchronize(stream);
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        fprintf(stderr, "CUDA error: %s\n", cudaGetErrorString(error));
        throw 42;
    }

    cudaMemcpyAsync(dataout, Gdata, size * sizeof(uint8_t), cudaMemcpyDeviceToHost, stream);
    cudaFree(Gdata);
    cudaStreamDestroy(stream);

    if(size - data_block_count*sizeof(struct blockdata)){
        printf("dobírka\n");
        int zbytek = size - data_block_count*sizeof(struct blockdata);
        int offset = data_block_count*sizeof(struct blockdata);
        Serpent cryptic(zbytek, _data + offset, dataout + offset, offset);
        cryptic.encrypt();
    }
}






