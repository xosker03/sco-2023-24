NUM_CORES = $(shell nproc)

CC = gcc
CXX = g++

NVCP = nvc++
NVCC = nvcc

CFLAGS = -O3 -I/opt/cuda/targets/x86_64-linux/include/
#CFLAGS = -g
CPPFLAGS = -std=c++17 -fpermissive
GCCFLAGS = -march=native -mtune=native -fopenmp -fopt-info-vec

CUDA_FLAGS = --device-c --restrict -Xcompiler #-allow-unsupported-compiler


EXECUTABLE_NAME = prog

HEADERS = $(wildcard *.h) #$(wildcard */*.h)
SOURCES = $(wildcard *.cpp) #$(wildcard */*.cpp)
SOURCES_CUDA = $(wildcard *.cu)
OBJECTS = $(SOURCES:.cpp=.o) $(SOURCES_CUDA:.cu=.o)

COMMAND = -o $(EXECUTABLE_NAME) $(OBJECTS)

LDFLAGS = -lpthread -lm #-lGL -lglfw -lGLEW

CUDA_ARCH = --generate-code arch=compute_50,code=sm_50 \
            --generate-code arch=compute_52,code=sm_52 \
            --generate-code arch=compute_53,code=sm_53 \
            --generate-code arch=compute_60,code=sm_60 \
            --generate-code arch=compute_61,code=sm_61 \
            --generate-code arch=compute_62,code=sm_62 \
            --generate-code arch=compute_70,code=sm_70 \
            --generate-code arch=compute_72,code=sm_72 \
            --generate-code arch=compute_75,code=sm_75

all: $(OBJECTS)
	$(NVCC) $(CFLAGS) -std=c++17 -Xcompiler -fopenmp $(COMMAND) $(LDFLAGS)

debug: CFLAGS = -g
debug: $(OBJECTS)
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)

run:
	./$(EXECUTABLE_NAME)

clean:
	rm -f $(EXECUTABLE_NAME)
	rm -f $(OBJECTS)

copytogpu:
	[ -d /home/pepa/mnt/gpu/realSCO ] && cp -v Makefile ${SOURCES} ${SOURCES_CUDA} ${HEADERS} /home/pepa/mnt/gpu/realSCO/


zip:
	zip -r xosker03.zip Makefile ${SOURCES} ${SOURCES_CUDA} ${HEADERS}

$(OBJECTS): $(HEADERS)

%.o: %.cpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) $(GCCFLAGS) -o $@ -c $<

%.o: %.cu
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) -fopenmp $(CUDA_ARCH) -o $@ -c $<



#-Wno-writable-strings
