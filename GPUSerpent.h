#ifndef GPUSerpent_H
#define GPUSerpent_H

#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "host.h"

void init_gpu(int gpu = 0);
void gpu_crypt(uint64_t size, char * _data, char * dataout, uint64_t ivOffset, int priority = 0, int gpu = 0);


#endif
