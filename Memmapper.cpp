#include "Memmapper.h"

#include "zaklad.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

Memmapper::Memmapper(const char * filename, bool pagelock)
{
	m_filename = std::string(filename);
    m_pagelock = pagelock;
}

Memmapper::Memmapper(std::string filename, bool pagelock)
{
    m_filename = filename;
    m_pagelock = pagelock;
}

Memmapper::~Memmapper()
{
	if(m_opened)
        mclose();
}


void Memmapper::create(uint64_t size)
{
    printblue("Memmapper:%s\t", m_filename.c_str());
    printblue("Creating:");
    if(m_opened){
        perror("Memmapper::create already opened");
        throw 5;
    }
    m_size = size;
    m_fd = open(m_filename.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (m_fd == -1) {
        perror("Memmapper::create::open error");
        throw 6;
    }

    // Nastavení velikosti souboru
    if (ftruncate(m_fd, m_size) == -1) {
        close(m_fd);
        perror("Memmapper::create::ftruncate error");
        throw 7;
    }
    printdgreen("Created ");
    map(PROT_READ | PROT_WRITE);
}


void Memmapper::read()
{
    mopen(PROT_READ);
}


void Memmapper::readwrite()
{
    mopen(PROT_READ | PROT_WRITE);
}

uint64_t Memmapper::size()
{
    return m_size;
}

char * Memmapper::data()
{
    return m_data;
}




void Memmapper::mopen(int prot)
{
    printblue("Memmapper:%s\t", m_filename.c_str());
    printblue("Openning:");

    if(m_opened){
        perror("Memmapper::mopen already opened");
        throw 4;
    }

    m_fd = open(m_filename.c_str(), O_RDWR);
    if (m_fd == -1) {
        perror("Memmapper::mopen::open error");
        throw 1;
    }

    // Získat velikost souboru
    __off_t s = lseek(m_fd, 0, SEEK_END);
    if(s < 0){
        perror("Memmapper::mopen::lseek error");
        throw 8;
    }
    m_size = s;

    printdgreen("Opened ");
    map(prot);
}

void Memmapper::map(int prot)
{
    printblue("Mapping:");

    //https://man7.org/linux/man-pages/man2/mmap.2.html

    int flags = MAP_SHARED_VALIDATE; //MAP_SHARED

    if(m_pagelock){
        flags |= MAP_LOCKED;
    }

    // Namapování souboru do paměti
    m_data = (char *) mmap(NULL, m_size, prot, flags, m_fd, 0);

    if (m_data == MAP_FAILED) {
        close(m_fd);
        perror("Memmapper::map::mmap error");
        throw 2;
    }

    m_opened = true;
    printdgreen("Mapped\n");
}



void Memmapper::mclose()
{
    if(m_opened){
        if (fsync(m_fd) == -1) {
            perror("Memmapper::mclose::fsync error");
            throw 8;
        }

        if (munmap(m_data, m_size) == -1) {
            perror("Memmapper::mclose::munmap error");
            throw 3;
        }
        close(m_fd);
        m_opened = false;
    }
}





