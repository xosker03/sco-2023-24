#ifndef Serpent_H
#define Serpent_H

#include <stdio.h>
#include <stdint.h>
#include "zaklad.h"
#include "host.h"
//https://www.cl.cam.ac.uk/~rja14/serpent.html


//CTR Mode
class Serpent{
public:
    Serpent(uint64_t size, char * _data, char * _dataout, uint64_t ivOffset);
	~Serpent();

    void init(uint64_t ivOffset);

    void encrypt();
    void decrypt();

private:
    void set_data(uint64_t size, char * _data);
    inline struct blockdata crypt(struct blockdata & inblock, uint64_t ivIdx);
    inline struct blockdata round(struct blockdata & inblock, int idx = -1);
    inline struct blockdata subkey_apply(struct blockdata & inblock, struct blockdata & subkey);
    inline struct blockdata sbox(struct blockdata & inblock, int idx);
    inline struct blockdata lin_trans(struct blockdata & inblock);
    inline struct blockdata final_round(struct blockdata & inblock);
    inline struct blockdata init_permut(struct blockdata & inblock);
    inline struct blockdata final_permut(struct blockdata & inblock);


    struct blockdata iv;

    uint64_t data_block_count;
    uint8_t end_bytes;
    struct blockdata * data;
    struct blockdata * dataout;
};



#endif
